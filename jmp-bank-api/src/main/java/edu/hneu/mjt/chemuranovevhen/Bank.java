package edu.hneu.mjt.chemuranovevhen;

public interface Bank {
    BankCard createBankCard(User user, BankCardType cardType);
}
