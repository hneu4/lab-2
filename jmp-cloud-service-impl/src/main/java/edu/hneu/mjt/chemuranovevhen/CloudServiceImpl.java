package edu.hneu.mjt.chemuranovevhen;

import java.time.LocalDate;
import java.util.*;

public class CloudServiceImpl implements Service {
    private final Map<String, BankCard> bankCards = new HashMap<>();
    private final Map<String, Subscription> subscriptions = new HashMap<>();
    private final List<User> users = new ArrayList<>();

    @Override
    public void subscribe(BankCard bankCard) {
        if (bankCard == null) {
            throw new IllegalArgumentException("BankCard cannot be null");
        }

        String cardNumber = bankCard.getNumber();
        Subscription subscription = new Subscription();
        subscription.setBankcard(cardNumber);
        subscription.setStartDate(LocalDate.now());

        subscriptions.put(cardNumber, subscription);
        bankCards.put(cardNumber, bankCard);

        users.stream()
                .filter(u -> !u.equals(bankCard.getUser()))
                .findFirst()
                .ifPresentOrElse(users::add, () -> users.add(bankCard.getUser()));
    }

    @Override
    public Optional<Subscription> getSubscriptionByBankCardNumber(String cardNumber) {
        return Optional.ofNullable(subscriptions.get(cardNumber));
    }

    @Override
    public List<User> getAllUsers() {
        return Collections.unmodifiableList(users);
    }
}
