package edu.hneu.mjt.chemuranovevhen;

public class CloudBankImpl implements Bank {
    @Override
    public BankCard createBankCard(User user, BankCardType cardType) {
        if (cardType == BankCardType.CREDIT) {
            CreditBankCard card = new CreditBankCard();
            card.setUser(user);
            card.setNumber(generateCardNumber());
            card.setCreditLimit(5000.0);
            return card;
        } else {
            DebitBankCard card = new DebitBankCard();
            card.setUser(user);
            card.setNumber(generateCardNumber());
            card.setBalance(1000.0);
            return card;
        }
    }

    private String generateCardNumber() {
        return "1234-5678-1234-" + ((int) (Math.random() * 10000));
    }
}
