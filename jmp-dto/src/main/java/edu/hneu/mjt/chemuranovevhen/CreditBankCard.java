//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package edu.hneu.mjt.chemuranovevhen;

public class CreditBankCard extends BankCard {
    private double creditLimit;

    public CreditBankCard() {
    }

    public double getCreditLimit() {
        return this.creditLimit;
    }

    public void setCreditLimit(double var1) {
        this.creditLimit = var1;
    }
}
