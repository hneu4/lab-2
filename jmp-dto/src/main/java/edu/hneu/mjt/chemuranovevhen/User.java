//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package edu.hneu.mjt.chemuranovevhen;

import java.time.LocalDate;

public class User {
    private String name;
    private String surname;
    private LocalDate birthday;

    public User() {
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String var1) {
        this.surname = var1;
    }

    public LocalDate getBirthday() {
        return this.birthday;
    }

    public void setBirthday(LocalDate var1) {
        this.birthday = var1;
    }
}
