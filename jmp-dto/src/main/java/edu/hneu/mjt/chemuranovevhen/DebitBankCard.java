//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package edu.hneu.mjt.chemuranovevhen;

public class DebitBankCard extends BankCard {
    private double balance;

    public DebitBankCard() {
    }

    public double getBalance() {
        return this.balance;
    }

    public void setBalance(double var1) {
        this.balance = var1;
    }
}
