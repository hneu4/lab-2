//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package edu.hneu.mjt.chemuranovevhen;

import java.time.LocalDate;

public class Subscription {
    private String bankcard;
    private LocalDate startDate;

    public Subscription() {
    }

    public String getBankcard() {
        return this.bankcard;
    }

    public void setBankcard(String var1) {
        this.bankcard = var1;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public void setStartDate(LocalDate var1) {
        this.startDate = var1;
    }
}
