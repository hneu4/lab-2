//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package edu.hneu.mjt.chemuranovevhen;

public class BankCard {
    private String number;
    private User user;

    public BankCard() {
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String var1) {
        this.number = var1;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User var1) {
        this.user = var1;
    }
}
