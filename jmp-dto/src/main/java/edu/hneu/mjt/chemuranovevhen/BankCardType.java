//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package edu.hneu.mjt.chemuranovevhen;

public enum BankCardType {
    CREDIT,
    DEBIT;

    private BankCardType() {
    }
}
