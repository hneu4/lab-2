module jmp.main {
    requires edu.hneu.mjt.chemuranovevhen.CloudBankImpl;
    requires edu.hneu.mjt.chemuranovevhen.CloudServiceImpl;
    requires jmp.bank.api;
    requires jmp.service.api;
    requires jmp.colud.bank.impl;
    requires jmp.dto;
}
