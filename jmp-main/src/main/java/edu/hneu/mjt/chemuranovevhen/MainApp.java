package edu.hneu.mjt.chemuranovevhen;

import java.time.LocalDate;
import java.util.Optional;

public class MainApp {
    public static void main(String[] args) {
        Service service = new CloudServiceImpl();
        Bank bank = new CloudBankImpl();

        User user = new User();
        user.setName("John");
        user.setSurname("Doe");
        user.setBirthday(LocalDate.of(1990, 1, 1));

        BankCard creditCard = bank.createBankCard(user, BankCardType.CREDIT);

        service.subscribe(creditCard);

        System.out.println("Registered Users:");
        for (User registeredUser : service.getAllUsers()) {
            System.out.println(" - " + registeredUser.getName() + " " + registeredUser.getSurname());
        }

        Optional<Subscription> subscription = service.getSubscriptionByBankCardNumber(creditCard.getNumber());

        if (subscription.isPresent()) {
            System.out.println("Subscription found for card " + creditCard.getNumber() + ":");
            System.out.println(" - Start date: " + subscription.get().getStartDate());
        } else {
            System.out.println("No subscription found for card " + creditCard.getNumber());
        }
    }
}
